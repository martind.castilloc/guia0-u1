#ifndef COORDENADA_H
#define COORDENADA_H

using namespace std;

class Coordenada {
    //Variables
    private:
        float x;
        float y;
        float z;

    //Constructor
    public:
    Coordenada();
    Coordenada(float x, float y, float z);

    //Metodos
    float get_x();
    float get_y();
    float get_z();
    void set_x(float x);
    void set_y(float y);
    void set_z(float z);
   
};
#endif