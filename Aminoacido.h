#ifndef AMINOACIDO_H
#define AMINOACIDO_H
using namespace std;
#include <string>
#include <list>
#include "Atomo.h"


class Aminoacido {
    private:
        string nombre;
        int numero;
        list<Atomo> atomos;

    //Constructor
    public:
    Aminoacido();
    Aminoacido(string nombre, int numero);

    //Metodos
    string get_nombre();
    int get_numero();
    list<Atomo> get_atomos();
    void set_nombre(string nombre);
    void set_numero(int numero);
    void add_atomo(Atomo atomo);
   
};
#endif