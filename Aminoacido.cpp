#include <iostream>
//using namespace std;
#include <list>
#include "Aminoacido.h"
#include "Atomo.h"

//Variables
Aminoacido::Aminoacido() {
    string nombre = "\0";
    int numero = 0;
    list<Atomo> atomos;
}

//Constructor
Aminoacido::Aminoacido(string nombre, int numero){
        this->nombre = nombre;
        this->numero = numero;
}

//Metodos
string Aminoacido::get_nombre(){
    return this->nombre;
}
int Aminoacido::get_numero(){
    return this->numero;
}    
list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}   
    
void Aminoacido::set_nombre(string nombre) {
    nombre = nombre;
}
void Aminoacido::set_numero(int numero) {
    numero = numero;
}
void Aminoacido::add_atomo(Atomo atomo){
    atomos.push_back(atomo);    
}