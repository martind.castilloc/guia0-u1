#ifndef CADENA_H
#define CADENA_H
using namespace std;
#include <string>
#include <list>
#include "Aminoacido.h"


class Cadena {
    
    private:
        string letra = "\0";
        list<Aminoacido> aminoacidos;

    //Constructor
    public:
        Cadena();
        Cadena(string letra);

        //Metodos
        string get_letra();
        list<Aminoacido> get_aminoacidos();
        void set_letra(string letra);
        void add_aminoacido(Aminoacido aminoacido);

};
#endif