#include <iostream>
#include "Coordenada.h"

using namespace std;

//Variables
Coordenada::Coordenada() {
    float x = 0.0;
    float y = 0.0;
    float z = 0.0;
}

//Constructor
Coordenada::Coordenada(float x, float y, float z){
    this->x = x;
    this->y = y;
    this->z = z;
}

//Metodos
float Coordenada::get_x(){
    return this->x;
}
float Coordenada::get_z(){
    return this->z;
}
float Coordenada::get_y(){
    return this->y;
}

void Coordenada::set_x(float x) {
    x = x;
}
void Coordenada::set_y(float y) {
    y = y;
}
void Coordenada::set_z(float z) {
    z = z;
}

