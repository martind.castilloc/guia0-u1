#include <iostream>
using namespace std;
#include <string>
#include <list>
#include "Atomo.h"
#include "Coordenada.h"

//Variables
Atomo::Atomo() {
    string nombre;
    int numero = 0;
    Coordenada coordenada;
}

//Constructor
Atomo::Atomo(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}

//Metodos
string Atomo::get_nombre(){
    return this->nombre;
}
int Atomo::get_numero(){
    return this->numero;
}
Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}

void Atomo::set_nombre(string nombre) {
    nombre = nombre;
}
void Atomo::set_numero(int numero) {
    numero = numero;
}
void Atomo::set_coordenada(Coordenada coordenada){
    coordenada = coordenada;
}