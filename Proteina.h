#ifndef PROTEINA_H
#define PROTEINA_H
using namespace std;
#include <string>
#include <list>
#include "Cadena.h"

class Proteina {
    //Variables
    private:
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadenas;

    //Constructor
    public:
        Proteina();
        Proteina(string nombre, string id);

        //Metodos
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
        void set_id(string id);
        void set_nombre(string nombre);
        void add_cadena(Cadena cadena);

};
#endif


