
#ifndef ATOMO_H
#define ATOMO_H
#include <iostream>
#include <string>
#include "Coordenada.h"

class Atomo {
    private:
        string nombre;
        int numero;
        Coordenada coordenada;

    public:
        //Constructor
        Atomo();
        Atomo(string nombre, int numero);

        //Metodos
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();
        void set_nombre(string nombre) ;
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
   
};
#endif