#include <iostream>
using namespace std;

#include <list>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

Proteina leer_datos(){

    string nombre, id, coordinate_x, coordinate_y, coordinate_z, letra;
    string cadenas, aminoaciods, atomos;
    cout << "Ingrese el nombre de la proteina" << endl;
    getline(cin, nombre);
    cout << "Ingrese el ID de la proteina" << endl;
    getline(cin, id);
    Proteina proteina = Proteina(nombre, id);
    cout << "¿Cuantas cadenas posee la proteina?" << endl;
    getline(cin, cadenas);
    for(int i = 0; i < stoi(cadenas); i++){
        cout << "Ingrese la cadena" << endl;
        getline(cin, letra);
        Cadena cadena = Cadena(letra);
        cout << "Ingrese la cantidad de aminoacidos de la cadena" << endl;
        getline(cin, aminoaciods);
        for(int j = 0; j < stoi(aminoaciods); j++){
            cout << "Ingrese el nombre del aminoacido " << j+1 << endl;
            getline(cin, nombre);
            Aminoacido aminoacido = Aminoacido(nombre, j + 1);
            cout << "Ingrese la cantidad de atomos del aminoacido "<< nombre << endl;
            getline(cin, atomos);
            for(int k = 0; k < stoi(atomos); k++){
                cout << "Ingrese el nombre del atomo" << k + 1 << endl;
                getline(cin, nombre);
                Atomo atomo = Atomo(nombre, k + 1);
                cout << "Ingrese la coordenada x del atomo" << endl;
                getline(cin, coordinate_x);
                cout << "Ingrese la coordenada y del atomo" << endl;
                getline(cin, coordinate_y);
                cout << "Ingrese la coordenada z del atomo" << endl;
                getline(cin, coordinate_z);
                Coordenada coor = Coordenada(stof(coordinate_x), stof(coordinate_y), stof(coordinate_z));
                atomo.set_coordenada(coor);
                aminoacido.add_atomo(atomo);
            }    
            cadena.add_aminoacido(aminoacido);  
        }  
            proteina.add_cadena(cadena);
    }
    return proteina;
}

void imprimir_datos_proteina(list<Proteina> proteinas){
	 
	int a=0;
	for(Proteina prot: proteinas){
		++a;
		int b=0;
		cout<<"········································Datos Proteinas··········"<<endl;
		cout<<endl;
		cout<<"Proteína: "<<a<<endl;
		cout<<"Nombre Proteina: "<<prot.get_nombre()<<endl;
		cout<<"Id Proteina: "<<prot.get_id()<<endl;
		cout<<"Numero de cadenas de la Proteina: "<<prot.get_cadenas().size()<<endl;
		cout<<endl;
        for(Cadena c: prot.get_cadenas()){
            cout << "Cadena " << c.get_letra() << ":" << endl;
            for (Aminoacido a: c.get_aminoacidos()){
                cout << "Aminoacido: " << a.get_numero() << "-" << a.get_nombre() << ":" << endl;
                for(Atomo at: a.get_atomos()){
                    cout << "" << at.get_nombre() << at.get_numero() << endl;
                    cout << "Coordenadas: ";
                    cout << at.get_coordenada().get_x();
                    cout << at.get_coordenada().get_y();
                    cout << at.get_coordenada().get_z() << endl;
                }
            }
        }
		cout<<"·······························"<<endl;	
	}
}


int main() {

    int opcion;
    list <Proteina> proteinas;
        
    do{
         
        cout << "\n\t ---------------------------- " << endl;
        cout << "\t ***** MENÚ DE OPCIONES ***** " << endl;
        cout << "\t ---------------------------- " << endl;
        cout << "\n  [1]  > Ingreso de datos." << endl;
        cout << "  [2]  > Imprimir proteina." << endl;
        cout << "  [3]  > Salir del programa." << endl;
        cout << "\n - Favor, elija una opción: " << endl;
        cin >> opcion;

        switch (opcion) {
    
            case 1: 
                cout << " Añade proteina \n" << endl;   
                proteinas.push_back(leer_datos());
                break;
                
            case 2:
                imprimir_datos_proteina(proteinas);
                break;
                   
            case 3:            
                cout << "\nHa salido del programa \n \n";
                return 0;
            
            default:
                cout << "\n\n\n\t--------------------------------" << endl;
                cout << "\tVALOR INVÁLIDO, INTENTE DE NUEVO" << endl;
                cout << "\t--------------------------------\n" << endl;
                break;  
     
        } 
    }
    while(opcion!=3);
    cout << "Ha salido del programa \n";
    return 0;
}
